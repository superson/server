FROM python:3

WORKDIR /app

ADD ./everyreview/setup.py /app/setup.py
RUN python3 /app/setup.py install

ADD ./everyreview /app

RUN chmod 644 /app/user-config.py

ENTRYPOINT ["/app/manage.py"]
CMD ["runserver"]