웹 서버 개발담당 : 윤성복 연수생

미디어 위키와 데이터 베이스의 정보를 합쳐 사용자에게 서비스 제공

# 최상위 디렉토리
    .
    ├── everyreview                # Django directory
    ├── .gitignore
    ├── .gitlab-ci.yml             # GitLab pipeline script
    ├── Dockerfile                 # 서버 도커 이미지 생성
    └── README.md

## everyreview 디렉토리
> Django project folder

    everyreview
    ├── everyreview                # Django base setting folder
    ├── wiki                       # wiki app folder
    ├── review                     # review app folder
    ├── everyreview_family.py      # pywikibot setting file
    ├── mediawiki_password.py      # mediawiki credential
    ├── manage.py                  # Django manage script
    ├── setup.py                   # project dependency file
    └── user-config.py             # pywikibot main setting file