from __future__ import with_statement
from setuptools import setup


install_requires = [
    'Django>=2.2.3',
    'pywikibot',
    'Jinja2==2.10.1',
    'psycopg2-binary==2.8.3',
    'sentry-sdk==0.13.1',
]
tests_require = [

]
docs_require = [

]

dev_require = tests_require + docs_require


def get_readme():
    try:
        with open('README.rst') as f:
            return f.read().strip()
    except IOError:
        return ''


setup(
    name='EveryReview',
    version='0.1',
    description='EveryReview Project',
    long_description=get_readme(),
    author='SuperSon',
    packages=(),
    package_data={},
    install_requires=install_requires,
    tests_require=tests_require,
    extras_require={
        'tests': tests_require,
        'docs': docs_require,
        'dev': dev_require,
    },
)
