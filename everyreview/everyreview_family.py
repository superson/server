from pywikibot import family


class Family(family.Family):
    name = 'everyreview'
    langs = {
        'ko': None,
    }

    def version(self, code):
        return '1.32.2'

    def hostname(self, code):
        return '13.124.126.139:8080'

    def scriptpath(self, code):
        return ''

    def isPublic(self, code):
        return False
