from django.test import TestCase
from django.urls import reverse


class AccountTest(TestCase):
    def test_signup_view(self):
        response = self.client.post(reverse('account:signup'), {'email': 'test@gmail.com',
                                                                'username': 'test_user',
                                                                'password': 'test-password',
                                                                'password_dup': 'test-password'})
        self.assertEqual(response.status_code, 200)
