from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth
from django.http.response import *
from django.http.request import *
from django.views.decorators.csrf import *


@csrf_exempt
def signup(request: HttpRequest):
    if request.method == 'POST':
        if request.content_type == 'application/json':
            payload = json.loads(request.body.decode('utf-8'))
        else:
            payload = request.POST

        if payload['password'] == payload['password_dup']:
            User.objects.create_user(email=payload['email'],
                                     username=payload['username'],
                                     password=payload['password'])
            return HttpResponse()
        return HttpResponseBadRequest()
    return render(request, 'account/signup.html', using='jinja2')


def signup_int(email: str, username: str, password: str):
    return User.objects.create_user(email=email, username=username, password=password)


@csrf_exempt
def login(request: HttpRequest):
    if request.method == 'POST':
        if request.content_type == 'application/json':
            payload = json.loads(request.body.decode('utf-8'))
        else:
            payload = request.POST
        user = auth.authenticate(request,
                                 username=payload['username'],
                                 password=payload['password'])
        if user is not None:
            auth.login(request, user)
            return HttpResponse()
        else:
            return HttpResponseBadRequest()
    else:
        return render(request, 'account/login.html')


@csrf_exempt
def logout(request: HttpRequest):
    auth.logout(request)
    return HttpResponse()
