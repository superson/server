import os

family_files['everyreview'] = os.path.abspath('./everyreview_family.py')

family = 'everyreview'
mylang = 'ko'
usernames['everyreview']['*'] = 'admin_bot'
password_file = os.path.abspath('./mediawiki_password.py')

#throttle setting
put_throttle = 0
