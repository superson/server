from django.urls import path
from . import views


app_name = 'wiki'
urlpatterns = [
    path('', views.index, name='index'),
    path('search/', views.search, name='search'),
    path('pages/<path:title>/edit/', views.edit, name='edit'),
    path('pages/<path:title>/refresh/', views.refresh, name='refresh'),
    path('pages/<path:title>/', views.page, name='page'),
    path('create/', views.create, name='create'),
]
