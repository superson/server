from django.shortcuts import render
from django.shortcuts import *
from django.http.response import *
from django.http.request import *
from django.template import Template, Context, engines, loader
from django.conf import settings
from django.views.decorators.csrf import *
from django.views.decorators.http import *
from .models import *
import wiki.pywikibot_wrapper as wikiAPI
import requests


def index(request):
    return render(request, 'wiki/index.html')


def search(request: HttpRequest):
    search_str = request.GET['search_str']
    search_gen = wikiAPI.search(search_str)
    results = []
    for result in search_gen:
        if not result.title().endswith('user-section'):
            results.append(result.title())
    context = {"results": results}
    if request.content_type == 'application/json':
        return JsonResponse(context, json_dumps_params={'ensure_ascii': False}, charset='utf-8')
    else:
        return render(request, 'wiki/search_results.html', context)


def page(request: HttpRequest, title):
    context = {}

    wiki_page = wikiAPI.Page.from_title(title)
    wiki_json = wiki_page.json

    template_html = wikiAPI.Page.from_title(wiki_json['template']).expand_text()
    template = get_template_from_string(template_html)

    context['title'] = title
    context['user_section'] = wiki_page.user_section_page().get_parsed_text()
    if wiki_page.exists():
        wiki_page_id = wiki_page.pageid
        refined_data = RefinedData.objects.filter(page_id=wiki_page_id).first()
        if refined_data:
            context.update(refined_data.info)
            context['site'] = refined_data.site
    return HttpResponse(template.render(context, request))


def get_template_from_string(string, name='jinja2'):
    return engines[name].from_string(string)


@csrf_exempt
def edit(request: HttpRequest, title: str):
    wiki_page = wikiAPI.Page.from_title(title)
    if not wiki_page.exists():
        return HttpResponseBadRequest()

    if request.method == 'GET':
        text = wiki_page.user_section_page().text
        context = {}
        context['title'] = title
        context['edit_text'] = text
        if request.content_type == 'application/json':
            return JsonResponse(context, json_dumps_params={'ensure_ascii': False}, charset='utf-8')
        else:
            return render(request, 'wiki/edit.html', context=context)
    elif request.method == 'POST' and request.user.is_authenticated:
        if request.content_type == 'application/json':
            payload = json.loads(request.body.decode('utf-8'))
        else:
            payload = request.POST
        edit_text = payload['edit_text']
        wiki_page.user_section_page().edit_page(request.user.id, edit_text)
        return HttpResponse()
    else:
        return HttpResponseBadRequest()


@csrf_exempt
def create(request: HttpRequest):
    if request.method == 'GET' and request.user.is_authenticated:
        return render(request, 'wiki/create.html')
    if request.method == 'POST' and request.user.is_authenticated:
        if request.content_type == 'application/json':
            payload = json.loads(request.body.decode('utf-8'))
        else:
            payload = request.POST

        title = payload['title']
        content = payload['content']

        main_page = wikiAPI.Page.from_title(title)
        if main_page.exists() or main_page.user_section_page().exists():
            return HttpResponseBadRequest()
        main_page.edit_page(request.user.id, """==json==
{{default_data|{{PAGENAME}}}}
""")
        main_page.user_section_page().edit_page(request.user.id, content)
        return HttpResponse()
    else:
        return HttpResponseBadRequest()


@require_GET
def refresh(request: HttpRequest, title: str):
    if not request.user.is_authenticated:
        return HttpResponseBadRequest()

    refined_data = RefinedData.from_title(title)
    if not refined_data:
        return HttpResponseBadRequest()
    else:
        response = requests.post('http://15.164.165.229:41925/update', {
            'uuid': refined_data.uuid,
            'category': refined_data.category,
            'token': 'E4j[uWqq~60&kf_Oe3CH',
        })
        if response.status_code == 200:
            return HttpResponse()
        else:
            return HttpResponseServerError()
