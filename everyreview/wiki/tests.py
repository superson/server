from django.test import TestCase
from django.urls import reverse


class HealthCheckTest(TestCase):
    def test_health_check(self):
        targets = ['wiki:index', ]
        for target in targets:
            response = self.client.get(reverse(target))
            self.assertEqual(response.status_code, 200)
