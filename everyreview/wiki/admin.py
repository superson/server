from django.contrib import admin, messages
from .models import *
import logging

import wiki.pywikibot_wrapper as wikiAPI
import pywikibot


@admin.register(RefinedData)
class RefinedDataAdmin(admin.ModelAdmin):
    list_display = ['page_title', 'uuid', 'category', 'page_id', 'update_date']
    actions = ['wiki_publish', 'wiki_publish_force']
    search_fields = ['page_title']

    def wiki_publish(self, request, queryset):
        success_sum = 0
        for refined_data in queryset.exclude(page_id=None).iterator():
            try:
                wikiAPI.update_wiki_from_data(refined_data=refined_data)
                success_sum += 1
            except pywikibot.InvalidTitle as exp:
                logging.warning(exp)
                self.message_user(request, str(exp), messages.ERROR)
        self.message_user(request, '{0}개의 데이터를 업데이트했습니다.'.format(success_sum))
    wiki_publish.short_description = '데이터베이스 정보로 위키 문서를 생성합니다.'

    def wiki_publish_force(self, request, queryset):
        success_sum = 0
        for refined_data in queryset.iterator():
            try:
                wikiAPI.update_wiki_from_data(refined_data=refined_data, force=True)
                success_sum += 1
            except pywikibot.InvalidTitle as exp:
                logging.warning(exp)
                self.message_user(request, str(exp), messages.ERROR)
        self.message_user(request, '{0}개의 데이터를 업데이트했습니다.'.format(success_sum))
    wiki_publish_force.short_description = '데이터베이스 정보로 위키 문서를 생성합니다.(Force)'

    def page_title(self, refined_data: RefinedData):
        return refined_data.page_title
    page_title.short_description = 'title'
