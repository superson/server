import pywikibot
import json
from django.utils import timezone
from typing import Union
from .models import RefinedData

site = pywikibot.Site()


def search(search_str):
    #TODO improve search
    return site.search(search_str, where='title')


class BasePage:
    _pw: pywikibot.Page
    _title: str

    def __init__(self, *, pwpage=None, title=None):
        if pwpage is None and title is None:
            raise ValueError('at least one of pwpage or title is mendatory')
        self._pw = pwpage
        self._title = title

    @classmethod
    def from_title(cls, title) -> Union['BasePage', pywikibot.Page]:
        return cls(title=title)

    def __getattr__(self, item):
        try:
            return self.__getattribute__(item)
        except AttributeError:
            try:
                return getattr(self.pw, item)
            except AttributeError:
                pass
            raise

    @property
    def pw(self) -> pywikibot.Page:
        if self._pw is None:
            self._pw = pywikibot.Page(site, self._title)
        return self._pw

    @property
    def title(self) -> str:
        if self._title is None:
            self._title = self.pw.title()
        return self._title

    def get_text_section(self, section):
        page = self.pw
        rev_id = page.latest_revision_id
        page_full_text = page.latest_revision.text
        site.loadrevisions(page, content=True, revids=page.latest_revision_id, section=section)
        text = page.text
        if page.latest_revision_id == rev_id:
            page.latest_revision.text = page_full_text
        else:
            # clear section only text cache
            page._revisions.pop(page.latest_revision_id)
        return text

    @property
    def json(self):
        text = self.get_text_section(1).replace('==json==', '', 1)
        json_text = site.expand_text(text=text, title=self.pw.title())
        return json.loads(json_text)

    def edit_page(self, edit_user, text: str, summary: str = None) -> None:
        self.pw.text = text
        self.pw.save(summary="{0}-{1}".format(edit_user, summary), minor=False)

    def get_parsed_text(self) -> str:
        req = site._simple_request(action='parse', title=self.title, text=self.pw.text, prop='text')
        data = req.submit()
        assert 'parse' in data, "API parse response lacks 'parse' key"
        assert 'text' in data['parse'], "API parse response lacks 'text' key"
        parsed_text = data['parse']['text']['*']
        return parsed_text


class Page(BasePage):
    def subpage(self, subtitle):
        new_title = f'{self.title}/{subtitle}'
        return Subpage.from_title(new_title)

    def user_section_page(self):
        return self.subpage('user-section')


class Subpage(BasePage):
    pass


def update_wiki_from_data(refined_data: RefinedData, force=False):
    if force is False and refined_data.page_id is not None and refined_data.page_id != 0:
        return

    page = pywikibot.Page(site, title=refined_data.page_title)

    page.text = """==json==
{{{{{0}_data|{{{{PAGENAME}}}}}}}}
""".format(refined_data.category)

    def update_page_id(updated_page, exception):
        if exception is not None:
            raise exception
        refined_data.page_id = updated_page.pageid
        refined_data.update_date = timezone.now()
        refined_data.save()

    page.save(summary='update from database', botflag=True, asynchronous=True, callback=update_page_id, quiet=True)
