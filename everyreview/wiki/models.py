from django.db import models
from django.contrib.postgres.fields import JSONField
from django.db.models.manager import *


class RefinedData(models.Model):
    uuid = models.UUIDField(primary_key=True)
    page_id = models.IntegerField(null=True, blank=True)
    update_date = models.DateTimeField()
    data = JSONField()

    objects: QuerySet

    @property
    def page_title(self):
        return self.data['data']['title']

    @property
    def category(self):
        return self.data['data']['category']

    @property
    def info(self):
        return self.data['data']['info']

    @property
    def site(self):
        return self.data['site']

    @classmethod
    def from_title(cls, title: str):
        try:
            return RefinedData.objects.get(data__title=title)
        except RefinedData.DoesNotExist:
            return None

