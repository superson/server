from review.models import *


class ExtReviewRouter:
    def db_for_read(self, model, **hints):
        if model in BaseExtReview.children():
            return 'crawling'
        return None

    def db_for_write(self, model, **hints):
        if model in BaseExtReview.children():
            return False
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1 in BaseExtReview.children() or obj2 in BaseExtReview.children():
            return False
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db == 'crawling':
            return False
        return None
