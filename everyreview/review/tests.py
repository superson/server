import datetime
from django.test import TestCase
from django.utils import timezone
from .models import Review


class DocumentModelTest(TestCase):
    def test_was_published_recently_with_past(self):
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        past_review = Review(pub_date=time)
        self.assertIs(past_review.was_published_recently(), False)

    def test_was_published_recently_with_future(self):
        time = timezone.now() + datetime.timedelta(days=30)
        future_review = Review(pub_date=time)
        self.assertIs(future_review.was_published_recently(), False)

    def test_was_published_recently_with_recent(self):
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_review = Review(pub_date=time)
        self.assertIs(recent_review.was_published_recently(), True)
