from django.urls import path
from . import views


app_name = 'review'
urlpatterns = [
    path('pages/<path:title>/', views.page, name='page'),
    path('like/<str:review_id>/', views.like, name='like'),
]
