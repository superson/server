import datetime
import pytz
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class Review(models.Model):
    user = models.ForeignKey(User, models.CASCADE)
    page_id = models.IntegerField()
    rating = models.FloatField()
    content = models.TextField()
    pub_date = models.DateTimeField()

    like_cnt = models.PositiveIntegerField(default=0)
    ref_name = models.TextField(null=True)
    ref_url = models.URLField(null=True)

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    def isliked(self, user: User):
        if user.is_anonymous:
            return False
        try:
            return ReviewLike.objects.get(review=self, user=user).is_liked
        except ReviewLike.DoesNotExist:
            return False

    def get_nickname(self):
        return self.user.get_username()

    def to_dict(self):
        d = {}
        d['id'] = str(self.id)
        d['nickname'] = self.get_nickname()
        d['rating'] = self.rating
        d['content'] = self.content
        d['date'] = self.pub_date.timestamp()
        d['like_cnt'] = self.like_cnt
        if self.ref_name:
            d['ref_name'] = self.ref_name
            d['ref_url'] = self.ref_url
        return d


class ReviewLike(models.Model):
    review = models.ForeignKey(Review, models.CASCADE)
    user = models.ForeignKey(User, models.CASCADE)
    is_liked = models.BooleanField(default=False)


class BaseExtReview(models.Model):
    review_id = models.IntegerField(primary_key=True)
    uuid = models.UUIDField(blank=True, null=True)
    source_site = models.CharField(max_length=20)
    source_id = models.CharField(max_length=32)
    user_name = models.CharField(max_length=20, blank=True, null=True)
    date = models.CharField(max_length=32, blank=True, null=True)
    review = models.CharField(max_length=999999999, blank=True, null=True)
    rating = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        abstract = True
        managed = False
        unique_together = (('review_id', 'source_site', 'source_id'),)

    def get_id(self):
        return "{0}-{1}-{2}".format(self.review_id, self.source_id, self.source_site)

    def get_like_cnt(self):
        try:
            return ExtReviewLikeCount.objects.get(ext_review_id=self.get_id()).like_cnt
        except ExtReviewLikeCount.DoesNotExist:
            return 0

    def to_dict(self):
        d = {}
        d['id'] = self.get_id()
        d['nickname'] = self.user_name
        d['rating'] = float(self.rating) / 2.0
        d['content'] = self.review
        for fmt in ('%Y.%m.%d, %H:%M', '%Y.%m.%d', '%Y년 %m월 %d일', '$m월 %d일'):
            try:
                review_datetime = datetime.datetime.strptime(self.date, fmt)
                break
            except ValueError:
                review_datetime = None
        if review_datetime:
            review_datetime_aware = timezone.make_aware(review_datetime, timezone=pytz.timezone('Asia/Seoul'))
            review_datetime_aware.replace(tzinfo=timezone.utc)
            d['date'] = review_datetime_aware.timestamp()
        else:
            d['date'] = datetime.datetime.fromtimestamp(0).timestamp()
        d['like_cnt'] = self.get_like_cnt()
        d['ref_name'] = self.source_site
        return d

    @classmethod
    def children(cls):
        return [ReviewMovie, ReviewRestaurant]


class ReviewRestaurant(BaseExtReview):
    class Meta(BaseExtReview.Meta):
        db_table = 'review_restaurant'


class ReviewMovie(BaseExtReview):
    class Meta(BaseExtReview.Meta):
        db_table = 'review_movie'


class ExtReviewLike(models.Model):
    ext_review_id = models.TextField(primary_key=True)
    user = models.ForeignKey(User, models.CASCADE)
    is_liked = models.BooleanField(default=False)


class ExtReviewLikeCount(models.Model):
    ext_review_id = models.TextField(primary_key=True)
    like_cnt = models.PositiveIntegerField(default=0)
