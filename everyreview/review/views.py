import wiki.pywikibot_wrapper as wikiAPI
from django.http.response import *
from django.http.request import *
from django.db.models import Avg
from django.views.decorators.csrf import *
from django.views.decorators.http import *
from django.contrib.auth.decorators import *
from django.shortcuts import render
from .models import *
from wiki.models import RefinedData


@csrf_exempt
def page(request: HttpRequest, title: str, ):
    if request.method == 'GET':
        page_id = wikiAPI.Page.from_title(title).pageid
        index = int(request.GET.get('index', 0))

        context = {}
        reviews = []
        query_page_review = Review.objects.filter(page_id=page_id)
        if query_page_review.exists():
            for review in query_page_review.order_by('-pub_date')[index*5:index*5+5]:
                d = review.to_dict()
                d['is_liked'] = review.isliked(request.user)
                reviews.append(d)
        context['reviews'] = reviews

        try:
            refined_data = RefinedData.objects.get(page_id=page_id)
        except RefinedData.DoesNotExist:
            pass
        else:
            if refined_data.category == 'movie':
                review_model = ReviewMovie
            elif refined_data.category == 'restaurant':
                review_model = ReviewRestaurant
            else:
                review_model = None

            if review_model is not None:
                query_ext_review = review_model.objects.filter(uuid=refined_data.uuid)
                for review in query_ext_review.order_by('-date')[index*5:index*5+5]:
                    d = review.to_dict()
                    for site in refined_data.site:
                        if review.source_site == site['site_name']:
                            d['ref_url'] = site['url']
                            d['is_liked'] = False
                            break
                    context['reviews'].append(d)

        if request.content_type == 'application/json':
            context['rating_avg'] = query_page_review.aggregate(Avg('rating'))['rating__avg']
            context['title'] = title
            return JsonResponse(context, json_dumps_params={'ensure_ascii': False}, charset='utf-8')
        else:
            return render(request, 'review/review_list.html', context, using='jinja2')
    elif request.method == 'POST':
        if not request.user.is_authenticated:
            return HttpResponseBadRequest()

        if request.content_type == 'application/json':
            payload = json.loads(request.body.decode('utf-8'))
        else:
            payload = request.POST

        review = Review()
        review.user = request.user
        review.page_id = wikiAPI.Page.from_title(title).pageid
        review.rating = float(payload['rating'])
        review.content = payload['content']
        review.pub_date = timezone.now()
        review.save()
        if request.content_type == 'application/json':
            return HttpResponse()
        else:
            return render(request, 'review/review_list.html', {'reviews': [review.to_dict()]}, using='jinja2')
    else:
        return HttpResponseBadRequest()


@require_POST
@csrf_exempt
def like(request: HttpRequest, review_id: str):
    if not request.user.is_authenticated:
        return HttpResponse(status=401)

    if request.content_type == 'application/json':
        payload = json.loads(request.body.decode('utf-8'))
    else:
        payload = request.POST

    is_like = str(payload['is_liked']).lower() == 'true'
    if review_id.isnumeric():
        review_like, created = ReviewLike.objects.get_or_create(review=Review.objects.get(id=review_id), user=request.user,
                                                         defaults={'is_liked': is_like})
    else:
        review_like, created = ExtReviewLike.objects.get_or_create(ext_review_id=review_id, user=request.user,
                                                            defaults={'is_liked': is_like})

    if review_like.is_liked is (not is_like):
        review_like.is_liked = is_like
        review_like.save()
    calc_like(review_like)
    return HttpResponse()


def calc_like(review_like):
    if type(review_like) is ReviewLike:
        review_like.review.like_cnt = ReviewLike.objects.filter(review=review_like.review, is_liked=True).count()
        review_like.review.save()
    elif type(review_like) is ExtReviewLike:
        ext_review_like_count, created = ExtReviewLikeCount.objects.get_or_create(ext_review_id=review_like.ext_review_id)
        ext_review_like_count.like_cnt = ExtReviewLike.objects.filter(ext_review_id=review_like.ext_review_id,
                                                                      is_liked=True).count()
        ext_review_like_count.save()
