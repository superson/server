from django.contrib import admin
from .models import Review


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ['user_id', 'page_id', 'rating', 'pub_date']
    search_fields = ['user_id', 'page_id']
